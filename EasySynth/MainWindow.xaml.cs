﻿using System.Windows;
using System.Windows.Media.Animation;
using System.Runtime.Serialization.Formatters.Binary;
using MidiLib;

namespace EasySynth
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MMInstrument inst = new MMInstrument(0);
        uint octave = 3;
        Storyboard sb;
        System.Windows.Media.SolidColorBrush myBrush = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(31, 31, 31));
        System.Collections.ArrayList record = new System.Collections.ArrayList();
        bool isRecording = false;
        System.Diagnostics.Stopwatch pauseTimer = new System.Diagnostics.Stopwatch();
        System.Diagnostics.Stopwatch duration = new System.Diagnostics.Stopwatch();
        int mils = 0;
        [System.Serializable()]
        public struct noteStruct
        {
            public int pauseDuration;
            public uint id;
            public uint velocity;
            public double duration;
            

            public noteStruct(uint id, uint velocity, double duration, int pauseDuration)
            {
                this.id = id;
                this.velocity = velocity;
                this.duration = duration;
                this.pauseDuration = pauseDuration;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void rect_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (isRecording)
            {
                if(pauseTimer.IsRunning)
                {
                    pauseTimer.Stop();
                    //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                    mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                    pauseTimer.Reset();
                }
              
                duration.Start();
            }
            inst.hitNote(octave * 12 + uint.Parse((sender as UIElement).Uid), System.Convert.ToUInt32(e.GetPosition((sender as IInputElement)).Y / 3));
            System.Windows.Shapes.Rectangle _rect = sender as System.Windows.Shapes.Rectangle;
            _rect.Fill = System.Windows.Media.Brushes.Gainsboro;
        }
        private void rectB_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (isRecording)
            {
                if (pauseTimer.IsRunning)
                {
                    pauseTimer.Stop();
                    //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                    mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                    pauseTimer.Reset();
                }
                duration.Start();
            }
            inst.hitNote(octave * 12 + uint.Parse((sender as UIElement).Uid), System.Convert.ToUInt32(e.GetPosition((sender as IInputElement)).Y / 2));
            System.Windows.Shapes.Rectangle _rect = sender as System.Windows.Shapes.Rectangle;
            _rect.Fill = System.Windows.Media.Brushes.Black;
        }

        private void rect_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            inst.releaseNote(octave * 12 + uint.Parse((sender as UIElement).Uid));
            if (isRecording)
            {
                duration.Stop();
                record.Add(new noteStruct(octave * 12 + uint.Parse((sender as UIElement).Uid), 100, duration.Elapsed.TotalMilliseconds, mils));
                pauseTimer.Start();
                duration.Reset();          
            }
            System.Windows.Shapes.Rectangle _rect = sender as System.Windows.Shapes.Rectangle;
            if((_rect.Fill == System.Windows.Media.Brushes.Black) || (_rect.Fill == myBrush))
            {
               
                _rect.Fill = myBrush;
            }
            else
            {
                _rect.Fill = System.Windows.Media.Brushes.White;
            }
        }

        private void rect_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {

            inst.releaseNote(octave * 12 + uint.Parse((sender as UIElement).Uid));
            //if (isrecording)
            //{
            //    duration.stop();
            //    messagebox.show(pausetimer.elapsedmilliseconds.tostring());
            //    record.add(new notestruct(octave * 12 + uint.parse((sender as uielement).uid), 100, duration.elapsed.totalmilliseconds, mils));
            //    pausetimer.start();
            //    duration.reset();
            //}
            System.Windows.Shapes.Rectangle _rect = sender as System.Windows.Shapes.Rectangle;
            if (_rect.Height <= 200)
            {
                _rect.Fill = myBrush;
            }
            else
            {
                _rect.Fill = System.Windows.Media.Brushes.White;
            }
        }

        private void octave_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            octave = System.Convert.ToUInt32(e.NewValue);
        }

        private void inst_combobox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            inst.changeTimbre((uint)(sender as System.Windows.Controls.ComboBox).SelectedIndex);
        }

        private void btn_rec_Click(object sender, RoutedEventArgs e)
        {
            isRecording = true;
            lbl_status.Content = "Recording";
            btn_stop.IsEnabled = true;
            btn_rec.IsEnabled = false;
            btn_open.IsEnabled = false;
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (!e.IsRepeat)
            {
                switch (e.Key)
                {
                    case System.Windows.Input.Key.Q:
                        inst.hitNote(octave * 12, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }
                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W1);
                        this.rect_W1.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.D2:
                        inst.hitNote(octave * 12 + 1, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B1);
                        this.rect_B1.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.W:
                        inst.hitNote(octave * 12 + 2, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W2);
                        this.rect_W2.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.D3:
                        inst.hitNote(octave * 12 + 3, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B2);
                        this.rect_B2.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.E:
                        inst.hitNote(octave * 12 + 4, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W3);
                        this.rect_W3.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.R:
                        inst.hitNote(octave * 12 + 5, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W4);
                        this.rect_W4.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.D5:
                        inst.hitNote(octave * 12 + 6, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B3);
                        this.rect_B3.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.T:
                        inst.hitNote(octave * 12 + 7, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W5);
                        this.rect_W5.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.D6:
                        inst.hitNote(octave * 12 + 8, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B4);
                        this.rect_B4.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.Y:
                        inst.hitNote(octave * 12 + 9, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W6);
                        this.rect_W6.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.D7:
                        inst.hitNote(octave * 12 + 10, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B5);
                        this.rect_B5.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.U:
                        inst.hitNote(octave * 12 + 11, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W7);
                        this.rect_W7.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.Z:
                        inst.hitNote(octave * 12 + 12, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W8);
                        this.rect_W8.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.S:
                        inst.hitNote(octave * 12 + 13, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B6);
                        this.rect_B6.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.X:
                        inst.hitNote(octave * 12 + 14, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W9);
                        this.rect_W9.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.D:
                        inst.hitNote(octave * 12 + 15, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B7);
                        this.rect_B7.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.C:
                        inst.hitNote(octave * 12 + 16, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W10);
                        this.rect_W10.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.V:
                        inst.hitNote(octave * 12 + 17, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W11);
                        this.rect_W11.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.G:
                        inst.hitNote(octave * 12 + 18, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B8);
                        this.rect_B8.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.B:
                        inst.hitNote(octave * 12 + 19, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W12);
                        this.rect_W12.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.H:
                        inst.hitNote(octave * 12 + 20, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B9);
                        this.rect_B9.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.N:
                        inst.hitNote(octave * 12 + 21, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W13);
                        this.rect_W13.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.J:
                        inst.hitNote(octave * 12 + 22, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("BlackKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_B10);
                        this.rect_B10.Fill = System.Windows.Media.Brushes.Black;
                        sb.Begin();
                        break;
                    case System.Windows.Input.Key.M:
                        inst.hitNote(octave * 12 + 23, 100);
                        if (isRecording)
                        {
                            if (pauseTimer.IsRunning)
                            {
                                pauseTimer.Stop();
                                //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                                mils = System.Convert.ToInt32(pauseTimer.ElapsedMilliseconds);
                                pauseTimer.Reset();
                            }

                            duration.Start();
                        }
                        sb = this.FindResource("WhiteKeyDown") as Storyboard;
                        Storyboard.SetTarget(sb, this.rect_W14);
                        this.rect_W14.Fill = System.Windows.Media.Brushes.Gainsboro;
                        sb.Begin();
                        break;
                }
            }
        }

        private void btn_play_Click(object sender, RoutedEventArgs e)
        {
            lbl_status.Content = "Playing";
            noteStruct note = new noteStruct();
            for (int i = 0; i < record.Count; i++)
            {
                note = (noteStruct)record[i];
                this.WaitNMilliSeconds(note.pauseDuration);
                inst.hitNote(note.id, note.velocity);
                this.WaitNMilliSeconds(note.duration);
                inst.releaseNote(note.id);
            }
            lbl_status.Content = "Finished";
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Storyboard sb;
            switch (e.Key)
            {
                case System.Windows.Input.Key.Q:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W1);
                    this.rect_W1.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.D2:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 1, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 1);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B1);
                    this.rect_B1.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.W:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 2, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 2);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W2);
                    this.rect_W2.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.D3:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 3, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 3);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B2);
                    this.rect_B2.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.E:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 4, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 4);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W3);
                    this.rect_W3.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.R:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 5, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 5);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W4);
                    this.rect_W4.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.D5:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 6, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 6);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B3);
                    this.rect_B3.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.T:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 7, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 7);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W5);
                    this.rect_W5.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.D6:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 8, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 8);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B4);
                    this.rect_B4.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.Y:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 9, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 9);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W6);
                    this.rect_W6.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.D7:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 10, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 10);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B5);
                    this.rect_B5.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.U:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 11, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 11);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W7);
                    this.rect_W7.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.Z:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 12, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 12);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W8);
                    this.rect_W8.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.S:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 13, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 13);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B6);
                    this.rect_B6.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.X:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 14, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 14);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W9);
                    this.rect_W9.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.D:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 15, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 15);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B7);
                    this.rect_B7.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.C:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 16, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 16);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W10);
                    this.rect_W10.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.V:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 17, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 17);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W11);
                    this.rect_W11.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.G:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 18, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 18);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B8);
                    this.rect_B8.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.B:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 19, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 19);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W12);
                    this.rect_W12.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.H:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 20, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 20);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B9);
                    this.rect_B9.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.N:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 21, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 21);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W13);
                    this.rect_W13.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.J:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 22, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 22);
                    sb = this.FindResource("BlackKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_B10);
                    this.rect_B10.Fill = myBrush;
                    sb.Begin();
                    break;
                case System.Windows.Input.Key.M:
                    if (isRecording)
                    {
                        duration.Stop();
                        //MessageBox.Show(pauseTimer.ElapsedMilliseconds.ToString());
                        record.Add(new noteStruct(octave * 12 + 23, 100, duration.Elapsed.TotalMilliseconds, mils));
                        pauseTimer.Start();
                        duration.Reset();
                    }
                    inst.releaseNote(octave * 12 + 23);
                    sb = this.FindResource("WhiteKeyUp") as Storyboard;
                    Storyboard.SetTarget(sb, this.rect_W14);
                    this.rect_W14.Fill = System.Windows.Media.Brushes.White;
                    sb.Begin();
                    break;
            }
        }

        private void btn_stop_Click(object sender, RoutedEventArgs e)
        {
            lbl_status.Content = "Successful";
            isRecording = false;
            btn_stop.IsEnabled = false;
            btn_save.IsEnabled = true;
            btn_delete.IsEnabled = true;
            btn_play.IsEnabled = true;
            pauseTimer.Stop();
            pauseTimer.Reset();
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            record = new System.Collections.ArrayList();
            lbl_status.Content = "Deleted";
            btn_play.IsEnabled = false;
            btn_rec.IsEnabled = true;
            btn_delete.IsEnabled = false;
            btn_open.IsEnabled = true;
        }

        private void WaitNMilliSeconds(double millisec)
        {
            if (millisec < 1) return;
            System.DateTime _desired = System.DateTime.Now.AddMilliseconds(millisec);
            while (System.DateTime.Now < _desired)
            {
                //System.Windows.Forms.Application.DoEvents();
                Application.Current.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
                                         new System.Action(delegate { }));
            }
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            var serializer = new BinaryFormatter();
            Microsoft.Win32.SaveFileDialog sfd = new Microsoft.Win32.SaveFileDialog();
            sfd.Filter = "Rec File (*.rec)|*.rec";
            if(sfd.ShowDialog() == true)
            {
                using (var stream = System.IO.File.OpenWrite(sfd.FileName))
                {
                    lbl_status.Content = "Writen!";
                    btn_open.IsEnabled = true;
                    btn_save.IsEnabled = false;
                    btn_play.IsEnabled = true;
                    btn_delete.IsEnabled = true;
                    serializer.Serialize(stream, record);
                }
            }
            else
            {
                lbl_status.Content = "Not written";
            }
        }

        private void btn_open_Click(object sender, RoutedEventArgs e)
        {
            var serializer = new BinaryFormatter();
            Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
            ofd.Filter = "Rec File (*.rec)|*.rec";
            if(ofd.ShowDialog() == true)
            {
                using (var stream = System.IO.File.OpenRead(ofd.FileName))
                {
                    if (record.Count > 0) record = new System.Collections.ArrayList();
                    record = (System.Collections.ArrayList)serializer.Deserialize(stream);
                    lbl_status.Content = "Opened!";
                    btn_play.IsEnabled = true;
                    btn_rec.IsEnabled = false;
                    btn_delete.IsEnabled = true;
                }
            }
            else
            {
                lbl_status.Content = "Not opened";

            }
        }
    }
}